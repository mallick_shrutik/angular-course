import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Dish } from '../shared/dish';
import { Params, ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";
import { DishService } from "../services/dish.service";
import { switchMap } from "rxjs/operators";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Comment } from "../shared/comment";
import { visibility,flyInOut,expand } from "../animations/app-animation";


@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style':'display:block;'
  },
  animations: [
    flyInOut(),
    visibility(),
    expand()
  ]
})

export class DishdetailComponent implements OnInit {

  dish: Dish;
  dishIds: string[];
  errMess: string;
  prev: string;
  next: string;
  commentForm: FormGroup;
  comment: Comment;
  dishcopy: Dish;
  visibility = 'shown';

  @ViewChild('fform') commentFormDirective;

  formErrors = {
    'author': '',
    'comment': ''
  };

  validationMessages = {
    'author': {
      'required': 'Name is required',
      'minlength': 'Author Name must be atleast 2 characters long',
      'maxlength': 'Author Name must be max 25 characters long'
    },
    'comment': {
      'required': 'Comment is required'
    }
  }


  constructor(private dishService: DishService,
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder,
    @Inject('BaseURL') public BaseURL) {

    this.createForm();
    this.comment = new Comment;
  }

  ngOnInit(): void {
    this.dishService.getDishIds()
      .subscribe((dishIds) => this.dishIds = dishIds);
    const id = this.route.params
      .pipe(switchMap((params: Params) => { this.visibility = 'hidden'; return this.dishService.getDish(params['id']); }))
      .subscribe((dish) => { this.dish = dish; this.dishcopy = dish; this.setPrevnext(dish.id); this.visibility = 'shown'; },
        errmess => this.errMess = <any>errmess);
  }

  createForm() {
    this.commentForm = this.fb.group({
      author: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      rating: 5,
      comment: ''
    });
    this.commentForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged();
  }

  onValueChanged(data?: any) {
    if (!this.commentForm) {
      return;
    }
    const form = this.commentForm;
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          if (control.errors.hasOwnProperty(key)) {
            this.formErrors[field] += messages[key] + ' ';
          }
        }
      }

    }
  }

  setPrevnext(dishId: string) {
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length]; // '+ index -1)%' is used to get last element if index is 0 in an array 
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length]; // and vice versa 
  }

  goBack(): void {
    this.location.back();
  }


  onSubmit() {
    this.comment = this.commentForm.value;
    if (this.commentForm.value) {

      this.comment.author = this.commentForm.value.author;
      this.comment.date = new Date().toISOString();
      this.comment.comment = this.commentForm.value.comment;
      this.comment.rating = this.commentForm.value.rating;
      this.dishcopy.comments.push(this.comment);
      this.dishService.putDish(this.dishcopy)
        .subscribe(dish => {
          this.dish = dish; this.dishcopy = dish;
        },
          errmess => { this.dish = null; this.dishcopy = null; this.errMess = <any>errmess; });
    }

    this.commentForm.reset({
      author: '',
      rating: 5,
      comment: '',
    });
    console.log(this.comment);
  }
}

// https://www.coursera.org/learn/angular/peer/yYV0Y/angular-forms-and-reactive-programming/review/FA_lniv1EeukLwopPFx-5w